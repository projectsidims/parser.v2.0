import { port } from './utils/config';
import App from './app';

const app = new App().app;

app.listen(port, () => console.log(`Server started on port ${port}`));