import express from 'express';

import Info from './modules/info';
import Parse from "./modules/parse";

import {validateUrl} from './utils/functions';

const router = express.Router();

router.post('/info', async (req, res) => {
    const {url} = req.body;
    if (validateUrl(res, url)) {
        await new Info(res, url).getInfo();
    }
});

router.post('/parse', async (req, res) => {
    const {url} = req.body;
    if (validateUrl(res, url)) {
        await new Parse(req, res, url).getParse();
    }
});

export default router