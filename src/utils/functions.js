import axios from 'axios';
import fs from 'fs';
import zipdir from 'zip-dir';

/**
 * Проверка на валидность URL
 * @param {Object} res
 * @param {String} url Ссылка на сайт
 * @returns {boolean} true/false
 */
function validateUrl(res, url) {
    const regUrl = '^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&\'\\(\\)\\*\\+,;=.]+$';
    if (!url) {
        res.status(400).json({'message': 'Поле URL пустое'});
        return false;
    } if (!(new RegExp(regUrl).test(url))) {
        res.status(400).json({'message': 'Не верный URL'});
        return false
    }
    return true;
}

/**
 * Сохранение файла
 * @param {string} file
 * @param {Object} response
 * @return {Promise<any>}
 */
function saveStreamFile(file, response) {
    return new Promise((resolve, reject) => {
        if (!response.pipe) return reject(file);
        response.pipe(fs.createWriteStream(decodeURIComponent(file)));
        response.on('end', () => resolve(response));
        response.on('error', () => reject(file));
    });
}

/**
 * Получение файла
 * @param {String} link
 * @return {Promise<any>}
 */
function requestStream(link) {
    return new Promise((resolve, reject) => {
        axios.get(link, {
            responseType: 'stream'
        }).then(response => resolve(response.data)).catch(error => reject(error))
    })
}

function createLinks(array) {
    const result = [];
    for (const link of array) {
        const format = link.split('.')[link.split('.').length - 1];
        if (['png', 'jpg', 'jpeg', 'gif', 'svg', 'ico', 'bmp', 'js', 'eot', 'woff2', 'ttf', 'woff', 'css', 'json', 'xml'].includes(format)) {
            const decode = decodeLink(link);
            const sort = sortFileFormat(decodeURI(link));
            const objact = createObjLink(link, decode, sort.folder + '/' + sort.fileName);
            result.push(objact);
        }
    }
    return getLikesValue(result);
}

function decodeLink(link) {
    return decodeURI(link)
}

function createObjLink(download, start, end) {
    return {
        download,
        start,
        end
    }
}

function getLikesValue(array) {
    const result = [];
    let counter = 1;
    for (const object of array) {
        if (!result.find(item => item.end === object.end)) {
            result.push(object);
        } else {
            counter++;
            const file = getFileName(object.end);
            object.end = object.end.replace(file.full, file.addSufix(counter));
            counter = 1;
            result.push(object);
        }
    }
    return result;
}

function getFileName(link) {
    const split = link.split('/');
    const file = split[split.length - 1];
    return {
        full: file,
        name: file.split('.')[0],
        ext: file.split('.')[1],
        addSufix(value) {
            return `${this.name}_${value}.${this.ext}`;
        }
    };
}

function sortFileFormat(link) {

    const format = link.split('.')[link.split('.').length - 1];
    const name = link.split('/')[link.split('/').length - 1];

    const schema = {
        'img': ['png', 'jpg', 'jpeg', 'gif', 'svg', 'ico', 'bmp'],
        'js': ['js'],
        'fonts': ['eot', 'woff2', 'ttf', 'woff'],
        'css': ['css'],
        'files': ['json', 'xml'],
        '/': ['html', 'php']
    };

    for (const folder in schema) {
        const formats = schema[folder];
        if (formats.includes(format)) {
            return {
                folder,
                fileName: name,
                format: formats
            }
        }
    }

    return {
        folder: '/',
        fileName: name
    };
}

function createZipProject(path, name) {
    zipdir(`./${path}`, {saveTo: `./${path}/${name}/${name}.zip`}, (error, buffer) => {
        if (error) console.log(error);
        return buffer;
    });
}

export {
    validateUrl,
    saveStreamFile,
    requestStream,
    createLinks,
    sortFileFormat,
    createZipProject
}