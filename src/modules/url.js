import {URL} from 'url';

class Url {
    constructor(url) {
        this.url = url;
        this.editUrl = `${new URL(this.url).protocol  }//${new URL(this.url).hostname}`;
        this.exclusion = '^.*(youtube|w.uptolike.com|tag.digitaltarget.ru|google|mc.yandex.ru|base64|mailto|vk.com|vk|fb|pinterest|facebook|tel|mail|s.ytimg.com|stats.g.doubleclick.net|i.ytimg.com|yt3.ggpht.com|static.doubleclick.net|fonts.gstatic.com|javascript)';
    }

    /**
     * Валидация URL
     * @param {Array} links Массив URL
     * @return {Array} Массив валидных ссылок
     */
    urlValidation(links) {
        const array = [];
        for (const link of links) {
            if (link) {
                if (!link.startsWith('http')) {
                    array.push(this.editUrl+link);
                } else {
                    array.push(link);
                }
            }
        }
        return array;
    }

    /**
     * Поверка на статус ссылки
     * @param {Array} links
     * @return {{success: Array, error: Array}}
     */
    static checkStatus(links) {
        const error = [];
        const success = [];
        for (const link of links) {
            const {status, url} = link;
            if (status >= 400) {
                error.push({status, url})
            } else  {
                success.push(url)
            }
        }
        return { error, success }
    }

    /**
     * Перебираем ссылки и отсеиваем ненужные
     * @param {Array} links Массив ссылок
     * @return {Array}
     */
    makeValidLinksArray(links) {
        const result = [];
        const regUrl = '^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&\'\\(\\)\\*\\+,;=.]+$';
        for (const link of links) {
            if (new RegExp(regUrl).test(link)) {
                if (link !== this.url && !new RegExp(this.exclusion).test(link)) {
                    const uri = new URL(link);
                    result.push( `${uri.protocol}//${uri.hostname + uri.pathname}` )
                }
            }
        }
        return result;
    }
}

export default Url;