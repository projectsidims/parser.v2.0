import {URL} from "url";
import PuppeteerInit from "./puppeteer";
import Directories from './directories';
import Download from './download';
import Url from "./url";

import {directory} from '../utils/config';
import {createZipProject} from "../utils/functions";

class Parse extends PuppeteerInit {
    constructor(req, res, url) {
        super();
        this.res = res;
        this.req = req;
        this.url = url;
        this.path = directory.path;
        this.name = new URL(this.url).hostname;
    }

    async getParse() {
        // Инициализация puppeteer. Возвращает объект браузера и страницы
        await this.initPuppeteer();

        // Получение ссылок
        const links = await this.networkPage();

        // Инициализация страницы
        await this.initPage(this.res, this.url);

        // Создание нужных директорий
        new Directories(this.url).createGlobalFolder().createSiteFolder().createFilesFolders();

        // Скачиваем HTML страницы
        const html = await this.downloadIndexFile();
        new Download(this.url).downloadHtml(html);

        // Закрываем puppeteer
        this.closeBrowser();

        // Поверка статуса
        const {error, success} = Url.checkStatus(links);

        // Валидация ссылок
        const urls = new Url(this.url).makeValidLinksArray(success);

        // Скачивание assets файлов
        const logs = await new Download(this.url).downloadAssetsFiles(this.res, urls);

        // Создание архива
        createZipProject(this.path, this.name);

        // Отправка клиенту логов о процессе скачивания
        const archive = `${this.req.protocol}://${this.req.headers.host}/${this.name}/${this.name}.zip`;
        this.sendInfo(archive, logs, error);
    }

    /**
     * Отправка информации клиенту
     * @param {String} archive archive сайта
     * @param {Array} logs Логи
     * @param {Array} error
     * @return {undefined}
     */
    sendInfo(archive, logs, error) {
        this.res.status(200).json({
            archive,
            logs,
            error
        })
    }
}

export default Parse;