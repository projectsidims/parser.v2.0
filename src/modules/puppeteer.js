import puppeteer from 'puppeteer';
import {puppeteerConf} from '../utils/config';

class PuppeteerInit {
    constructor() {
        this.options = {
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-accelerated-2d-canvas',
                '--disable-gpu'
            ],
            defaultViewport: {
                width: puppeteerConf.width,
                height: puppeteerConf.height
            }
        };
    }

    /**
     * Инициализация puppeteer
     * @return {undefined}
     */
    async initPuppeteer() {
        this.browser = await puppeteer.launch(this.options);
        this.page = await this.browser.newPage();
    };

    /**
     * Закрывает страницу и браузер
     * @return {undefined}
     */
    closeBrowser() {
        this.page.close();
        this.browser.close();
    }

    /**
     * Получение ссылок [images, fonts, css, js]
     * @return {Array} массив ссылок
     */
    async networkPage() {
        const links = [];
        await this.page.on('response', response => {
            links.push({
                status: response.status(),
                url: response.url()
            });
        });
        return links;
    }

    /**
     * Инициализация страницы
     * @param {Object} res
     * @param {String} url URL сайта
     * @return {Promise<Object|String>}
     */
    async initPage(res, url) {
        try {
            return await this.page.goto(url, {
                waitUntil: 'domcontentloaded'
            });
        } catch (error) {

            this.closeBrowser();

            switch (error.message) {
                case `net::ERR_NAME_NOT_RESOLVED at ${url}`:
                    return res.status(404).json({message: 'Сайт не найден'});
                case 'Navigation Timeout Exceeded: 30000ms exceeded':
                    return res.status(400).json({message: 'Долго нет ответа от сервера'});
                default:
                    return error;
            }

        }
    }

    /**
     * Создание скриншота страницы
     * @returns {!Promise<!Buffer|!String>|!Promise<string|!Buffer>|*} Base64
     */
    async createScreenshot() {
        return await this.page.screenshot({type: 'jpeg', encoding: 'base64'});
    }

    /**
     * Получение фавикон
     * @returns {Promise<Array>} Массив
     */
    async getFavicon () {
        return await this.page.evaluate(() => {
            return [...document.querySelectorAll('link[rel*="icon"]')].map(link => link.getAttribute('href'));
        });
    }

    /**
     * Получение Title сайта
     * @return {Promise<String>}
     */
    async getTitle() {
        return await this.page.evaluate(() => document.title);
    }

    /**
     * Скачивание index файла
     * @return {String}
     */
    async downloadIndexFile() {
        return await this.page.content();
    }
}

export default PuppeteerInit;
