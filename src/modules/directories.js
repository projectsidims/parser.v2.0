import {URL} from "url";
import fs from 'fs';

import {directory} from '../utils/config';

class Directories {
    constructor(url) {
        this.url = url;
        this.path = directory.path;
        this.name = new URL(this.url).hostname;
    }

    createGlobalFolder() {
        !fs.existsSync(`./${this.path}`) && fs.mkdirSync(`./${this.path}`);
        return this;
    }

    createSiteFolder() {
        !fs.existsSync(`./${this.path  }/${  this.name}`) && fs.mkdirSync(`./${this.path  }/${  this.name}`);
        return this;
    }

    createFilesFolders() {
        for (const folder of ['img', 'css', 'js', 'fonts', 'files']) {
            const link = `./${this.path}/${this.name}/${folder}`;
            !fs.existsSync(link) && fs.mkdirSync(link);
        }
    }
}

export default Directories;