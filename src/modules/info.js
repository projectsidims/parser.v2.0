import PuppeteerInit from './puppeteer';
import Url from './url';

class Info extends PuppeteerInit {
    constructor(res, url) {
        super();
        this.res = res;
        this.url = url;
    }

    async getInfo() {
        // Инициализация puppeteer
        await this.initPuppeteer();

        // Инициализация страницы
        await this.initPage(this.res, this.url);

        // Создание скриншота
        const screenshot = await this.createScreenshot();

        // Получение favicon
        const favicon = new Url(this.url).urlValidation(await this.getFavicon());

        // Получение title
        const title = await this.getTitle();

        // Закрытие браузера
        this.closeBrowser();

        // Отправка клиенту
        this.sendInfo(title, favicon, screenshot);
    }

    /**
     * Отправка информации клиенту
     * @param {String} title Title сайта
     * @param {Array} favicon Favicon сайта
     * @param {String} screenshot Screenshot сайта
     * @return {undefined}
     */
    sendInfo(title, favicon, screenshot) {
        this.res.status(200).json({
            url: this.url,
            title,
            favicon,
            screenshot
        })
    }
}

export default Info;