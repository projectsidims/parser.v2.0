import {URL} from 'url';
import pretty from 'pretty';
import stripHtmlComments from 'strip-html-comments';
import fs from 'fs';
import path from 'path';

import {directory} from '../utils/config';
import {saveStreamFile, requestStream, createLinks} from '../utils/functions';

class Download {
    constructor(url) {
        this.url = url;
        this.path = directory.path;
        this.name = new URL(this.url).hostname;
    }

    /**
     * Скачивание index.html
     * @param {String} html html страницы
     * @return {undefined}
     */
    downloadHtml(html) {
        const result = stripHtmlComments(pretty(html, {ocd: true}));
        fs.writeFileSync(`./${this.path}/${this.name}/index.html`, result);
    }

    /**
     * Скачивание Assets файлов
     * @param {Object} res
     * @param {Array} links
     * @return {Promise<Array>} массив логов
     */
    async downloadAssetsFiles(res, links) {
        const array = createLinks(links);
        const logs = [];
        for (const link of array) {
            const {download, end} = link;
            const stream = await requestStream(download).catch(error => console.log(error));
            const join = path.join(`./${this.path}/${this.name}/${end}`);
            const save = await saveStreamFile(join, stream).catch(error => console.log(error));
            logs.push({
                link: save.responseUrl,
                status: save.statusCode
            })
        }
        return logs;
    }
}

export default Download;