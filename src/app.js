import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import path from 'path';

import {directory} from './utils/config'
import router from './router';

class App {
    constructor() {
        this.app = express();
        this.middleware();
    }

    middleware() {
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use(express.static(path.join(__dirname, `../${directory.path}`)));
        this.app.use('/api', router);
    }
}

export default App;