#Парсер
Статус: Development

###Содержание
* [Установка](#Установка)
* [Зависимости](#Зависимости)
* [Начало](#Начало)
    * [Информация о сайте](#Информация)
    * [Парсинг](#Парсинг)

## Установка
```
git clone https://projectsidims@bitbucket.org/projectsidims/parser.v2.0.git - Клонируем репозиторий
    
cd /path/to/your/repo - Переходим в каталог
    
npm install - Устанавливаем зависимости
```

## Зависимости
+ [express.js](https://www.npmjs.com/package/express);
+ [puppeteer](https://www.npmjs.com/package/puppeteer);
+ [url](https://www.npmjs.com/package/url);
+ [fs](https://www.npmjs.com/package/fs);
+ [@babel/runtime](https://www.npmjs.com/package/@babel/runtime);
+ [axios](https://www.npmjs.com/package/axios);
+ [body-parser](https://www.npmjs.com/package/body-parser);
+ [cors](https://www.npmjs.com/package/cors);
+ [dotenv](https://www.npmjs.com/package/dotenv);
+ [path](https://www.npmjs.com/package/path);
+ [pretty](https://www.npmjs.com/package/pretty);
+ [strip-html-comments](https://www.npmjs.com/package/strip-html-comments);

## Начало
#### Информация
```
POST /api/info
```
    
Пример запроса:

```javascript
axios.post('/api/info', {url: "url_site"}).then(res => {
    console.log(res);
});
```

Response:

```
{
  "url": "https://google.com/",
  "title": "Google",
  "favicon": [
    "https://google.com/favicon/ico.png"
  ],
  "screen": "EAPwD9JoiICIiAiIgIiICIi... base64"
}
```
#### Парсинг
```
POST /api/parse
```
    
Пример запроса:
```javascript
axios.post('/api/parse', {url: "url_site"}).then(res => {
    console.log(res);
});
```
Response:
```
{
    "archive": "http://localhost:3000/google/google.zip",
    "logs": [
        {
            "link": "https://www.gstatic.com/inputtools/images/tia.png",
            "status": 200
        },
        {
            "link": "https://ssl.gstatic.com/gb/images/i1_1967ca6a.png",
            "status": 200
        }
    ]
}
```